# 1. Stage - Base Node  (Образ alpine является производным проекта Alpine Linux, и это помогает уменьшить размер образа)
FROM node:14-alpine AS base
# Cоздадим подкаталог node_modules в каталоге /var/www вместе с каталогом app
RUN mkdir -p /var/www/tree/node_modules && chown -R node:node /var/www/tree
# Перейдём в рабочий каталог
WORKDIR /var/www/tree
# Копируем package.json 
COPY package*.json ./
# Установить все зависимости 
RUN npm set progress=false && \
    npm config set depth 0 && \
    npm install
# 2. Stage 
FROM node:14-alpine AS builder
# Перейдём в рабочий каталог
WORKDIR /var/www/tree
# Копируем папку модулей
COPY --from=base /var/www/tree/node_modules /var/www/tree/node_modules
# Копируем весь проект 
COPY ./ /var/www/tree
# Для работы с knex
RUN npm i knex -g
# Используем польозвателя node 
USER node
# В случаи копирования файлы будут принадлежать пользователю node
COPY --chown=node:node . . 

# Запуск в dev режиме
ENTRYPOINT ["npm", "run", "dev"]
# Прослушиваем порт
EXPOSE 4000
