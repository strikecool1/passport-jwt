const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const { knex } = require('../knexfile');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

passport.use(
    new JWTstrategy({
            secretOrKey: process.env.SECRET_KEY ||'TOP_SECRET',
            jwtFromRequest: ExtractJWT.fromUrlQueryParameter('secret_token')
        },
        async (token, done) => {
            try {
                return done(null, token.user);
            } catch (error) {
               console.log(error);
            }
        }
    )
);

const user = {
    email: 'test-user',
    password: 'my-password',
    id: 1
}

passport.use('signup', new localStrategy(
    async (email, password, done) => {
        try {
            const user = await knex('users').insert({
                email,
                password
            });
            return done(null, user);
        } 
        catch (error) {
            console.log(error);
        }
    })
)

// Регистрация
/*passport.use(
    'signup',
    new localStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        async (email, password, done) => {
            try {
                const user = await knex('users').insert({
                    email,
                    password
                });
                return done(null, user);
            } 
            catch (error) {
                console.log(error);
            }
        }),
);
*/
// Авторизация
passport.use(
    'signin',
    new localStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        async (email, password, done) => {
            try {
                const user = await knex('users').where({
                    email : email
                }).first();

                if (!user) {
                    return done(null, false, {
                        message: 'User not found'
                    });
                }
                return done(null, user, {
                    message: 'Logged in Successfully'
                });
            } 
            catch (error) {
                console.log(error);
            }
        }
    )
);