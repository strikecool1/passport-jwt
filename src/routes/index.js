const { Router } = require("express");
require('dotenv').config();
const router = Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');

// ------------------ Endpoints ------------------------
router.get('/', (req, res) => {
    res.status(200).send('Hello world');
});

// register
router.post(
  '/signup',
  passport.authenticate('signup', {
      session: false
  }),
  async (req, res, next) => {
      console.log('test');
      res.json({
          message: 'Signup successful',
          user: req.user
      });
  }
);
// login
router.post('/signin', async (req, res, next) => {
  passport.authenticate(
          'signin',
          async (err, user, info) => {
              try {
                  if (err || !user) {
                      const error = new Error('An error occurred.');
                      return next(error);
                  }
                  req.login(
                      user, {
                          session: false
                      },
                      async (error) => {
                          if (error) return next(error);
                          const body = {
                              _id: user._id,
                              email: user.email
                          };
                          const token = jwt.sign({
                                  user: body
                              },
                              process.env.SECRET_KEY ||'TOP_SECRET');
                          return res.json({
                              token
                          });
                      }
                  );
              } catch (error) {
                  return next(error);
              }
          }
      )
      (req, res, next);
});


module.exports = router;