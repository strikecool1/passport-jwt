const express = require("express");
const app = express();
require('dotenv').config();
const bp = require("body-parser");
const indexRouter = require("./routes/index");

// Парсинг body
app.use(bp.json());
app.use(bp.urlencoded({ extended: false }));
app.use("/", indexRouter);
// Ошибка 404
app.use((req, res, next) => {
    let err = new Error("Страница не найдена");
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send(err)
});

const portSettings = process.env.PORT || 4000;
app.listen(portSettings, () => console.log(`Сервер Запущен на ${portSettings}`));