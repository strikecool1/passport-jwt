require('dotenv').config();

module.exports = {
  knex: {
    client: 'pg',
    connection: {
      database: process.env.DB_NAME || 'auth_users',
      user:     process.env.DB_NAME || 'admin',
      password: process.env.DB_PASSWORD || 'admin',
      port:     process.env.DB_PORT || 5432,
      charset: 'utf8',
      timezone: 'Europe/Moscow',
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: __dirname + "/database/migrations",
      tableName: 'migrations'
    }
  },
};
